package lab03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FooTests {
    Foo foo;

    @BeforeEach                                     //provede se pred kazdym testem
    public void SingleTestSetUp() {
        foo = new Foo();
    }

    // NázevMetody_TestovanýStav_OčekávanýVýstup
    @Test
    public void ReturnNumber_ReturningSpecificNumber_Five() {
        // arrange
        int expected_value = 5;
        // act
        int result = foo.returnNumber();
        // assert
        Assertions.assertEquals(expected_value, result);
    }

    @Test
    public void GetNum_GetDefaultNumber_Zero() {
        // arrange
        int expected_value = 0;
        // act
        int result = foo.getNum();
        // assert
        Assertions.assertEquals(expected_value, result);
    }

    @Test
    public void Increment_NumConstantGettingGreaterByOne_GreaterNumberByOne() {
        // arrange
        int expected_value = foo.getNum() + 1;
        // act
        foo.increment();
        // assert
        Assertions.assertEquals(expected_value, foo.getNum());
    }

    @Test
    public void ExceptionThrown_GetSameMessage_ExceptionMessage() throws Exception {
        // arrange
        // act & assert
        Assertions.assertThrows(Exception.class, () -> {
            foo.exceptionThrown();
        });

    }
}
