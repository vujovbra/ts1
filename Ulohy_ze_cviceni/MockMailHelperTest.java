package cz.cvut.fel.ts1.refactoring;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class MockMailHelperTest {


//    mock(classToMock)
//              – vytváří mock dané třídy nebo interface
//    mock(DBManager.class)
//  when(methodCall)
//          – umožňuje definovat chování dané metody (Stub konfigurace)
//    when(mockedDbManager.findMail(anyInt())).thenReturn(mailToReturn);
//    verify(mock)
//             – umožňuje ověřit, zda metoda mocku byla volána (i dle parametrů)
//    verify(mockedDbManager,times(2)).saveMail(any(Mail.class));


    @Test
    public void sendMail_validEmail_saveEmailToDB() {
        DBManager mockedDBManager = mock(DBManager.class);
        MailHelper mailHelper = new MailHelper(mockedDBManager);
        int mailID = 1;
        Mail mailToReturn = mock(Mail.class);
        when(mailToReturn.getTo()).thenReturn("");
        when(mailToReturn.getBody()).thenReturn("");
        when(mailToReturn.getSubject()).thenReturn("");
        when(mockedDBManager.findMail(anyInt())).thenReturn(mailToReturn);


        mailHelper.sendMail(mailID);

        verify(mockedDBManager,times(1)).findMail(mailID);
        verify(mockedDBManager,times(1)).saveMail(any(Mail.class));

//        verify(mockedDBManager).findMail(anyInt());

    }
}
